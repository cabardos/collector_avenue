<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collections extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function users(){
        return $this->belongsToMany(User::Class,'user_collection', 'user_id', 'collection_id');
    }
}