<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    public function index(){
        return Genre::all();
    }

    public function show($id){
        return Genre::find($id);
    }

}
