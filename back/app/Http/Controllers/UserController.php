<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 

class UserController extends Controller
{
    public function index(){
        return User::with('genres', 'collections', 'object', 'bd', 'event')->get();
    }

    public function show($id){
        return User::with('genres', 'collections', 'object', 'bd', 'event')->where('users.id', $id)->get();
    }
}
