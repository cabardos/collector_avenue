<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GenreBd;

class UserGenreController extends Controller
{
    public function index(){
        return GenreBd::all();
    }

    public function show($id){
        return GenreBd::find($id);
    }

    public function store(Request $request){
        return GenreBd::create($request->all());
        // return $request;
    }

    public function update(Request $request, $id){
        $events = GenreBd::findOrFail($id);
        $events->update($request->all());
        
        return $events;
    }

    public function delete(Request $request,$id){
        $events = GenreBd::findOrFail($id);
        $events->delete();
        
        return response()->json(null, 204);
    }
}
