<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;

class EventsController extends Controller
{
    public function index(){
        return User::with('coupdefoudre');
    }

    public function show($id){
        return Events::find($id);
    }

    public function store(Request $request){
        return Events::create($request->all());
    }

    public function update(Request $request, $id){
        $events = Events::findOrFail($id);
        $events->update($request->all());
        
        return $events;
    }

    public function delete(Request $request,$id){
        $events = Events::findOrFail($id);
        $events->delete();
        
        return response()->json(null, 204);
    }
}
