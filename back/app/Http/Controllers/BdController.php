<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bd;


class BdController extends Controller
{
    public function index(){
        return Bd::all();
    }

    public function show($id){
        return Bd::find($id);
    }

    public function store(Request $request){
        return Bd::create($request->all());
    }

    public function update(Request $request, $id){
        $events = Bd::findOrFail($id);
        $events->update($request->all());
        
        return response()->json($response, 201);
    }

    public function delete(Request $request,$id){
        $events = Bd::findOrFail($id);
        $events->delete();
        
        return response()->json(null, 204);
    }
}
