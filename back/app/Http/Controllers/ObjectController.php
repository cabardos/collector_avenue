<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Object;

class ObjectController extends Controller
{
    public function index(){
        return Object::all();
    }

    public function show($id){
        return Object::find($id);
    }

    public function store(Request $request){
        return Object::create($request->all());
    }

    public function update(Request $request, $id){
        $events = Object::findOrFail($id);
        $events->update($request->all());
        
        return $events;
    }

    public function delete(Request $request,$id){
        $events = Object::findOrFail($id);
        $events->delete();
        
        return response()->json(null, 204);
    }
}
