<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CoupDeFoudre;

class CoupDeFoudreController extends Controller
{
    public function index(){
        return CoupDeFoudre::all();
    }

    public function show($id){
        return CoupDeFoudre::find($id);
    }

    public function store(Request $request){
        return CoupDeFoudre::create($request->all());
    }

    public function update(Request $request, $id){
        $events = CoupDeFoudre::findOrFail($id);
        $events->update($request->all());
        
        return response()->json($response, 201);
    }

    public function delete(Request $request,$id){
        $events = CoupDeFoudre::findOrFail($id);
        $events->delete();
        
        return response()->json(null, 204);
    }
}
