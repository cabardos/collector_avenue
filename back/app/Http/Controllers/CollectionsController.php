<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Collections;

class CollectionsController extends Controller
{
    public function index(){
        return Collections::all();
    }

    public function show($id){
        return Collections::find($id);
    }

    public function store(Request $request){
        return Collections::create($request->all());
    }

    public function update(Request $request, $id){
        $events = Collections::findOrFail($id);
        $events->update($request->all());
        
        return response()->json($response, 201);
    }

    public function delete(Request $request,$id){
        $events = Collections::findOrFail($id);
        $events->delete();
        
        return response()->json(null, 204);
    }
}
