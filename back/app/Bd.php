<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bd extends Model
{
    protected $fillable = [
        'name', 'numero', 'isbs', 'editeur', 'dessinateur', 'scenariste', 'remuse'
    ];

    public function users(){
        return $this->belongsToMany(Users::Class,'user_collection_bd');
    }
}
