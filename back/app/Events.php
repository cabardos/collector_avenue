<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = [
        'titre', 'date_start','date_end','place', 'event_detail', 'website_link', 'created_at', 'updated_at'
    ];

    public $timestamps = false;

    public function users(){
        return $this->belongsToMany(Users::Class,'genre_bd');
    }
}
