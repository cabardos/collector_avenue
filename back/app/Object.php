<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
    protected $fillable = [
        'name', 'description', 'dimension', 'material', 'EAN13_CO'
    ];

    public $timestamps = false;

    public function users(){
        return $this->belongsToMany(User::Class,'user_collection_object');
    }

}
