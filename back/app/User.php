<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable implements JWTSubject
{
    protected $fillable = [
        'name', 'email', 'password', 'is_verified'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function collections(){
        return $this->belongsToMany(Collections::Class, 'user_collection');
    }

    public function object(){
        return $this->belongsToMany(Object::Class,'user_collection_object');
    }

    public function bd(){
        return $this->belongsToMany(Bd::Class,'user_collection_bd');
    }

    public function event(){
        return $this->belongsToMany(Events::Class,'users_event');
    }

    public function genres(){
        return $this->belongsToMany(Genre::Class,'genre_bd');
    }
}