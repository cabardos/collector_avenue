<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoupDeFoudre extends Model
{
    protected $fillable = [
        'user_id1', 'user_id2'
    ];

    public function users(){
        return $this->belongsTo(User::class);
    }
}
