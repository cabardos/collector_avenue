<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenreBd extends Model
{
    protected $fillable = [
        'user_id',  'genre_id'
    ];

    public $timestamps = false;

}
