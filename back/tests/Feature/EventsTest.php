<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEventsIndex()
    {
        $response = $this->json('GET', '/api/collections/');
        $response
            ->assertStatus(200);
    }

    public function testEventsShow()
    {
        $response = $this->json('GET', '/api/collections/1');
        $response
            ->assertStatus(200);
    }

    public function testEventsAdd()
    {
        $response = $this->json('POST', '/api/collections/', ['name' => 'Test']);

        // var_dump($response); 

        $response->assertStatus(201);
            
    }
}
