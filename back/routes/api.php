<?php

use Illuminate\Http\Request;


// GENRES
Route::get('genres', 'GenreController@index');
Route::get('genres/{id}', 'GenreController@show');

// GENRE BD
Route::get('genrebd', 'UserGenreController@index');
Route::get('genrebd/{id}', 'UserGenreController@show');
Route::post('genrebd', 'UserGenreController@store');
Route::put('genrebd/{id}', 'UserGenreController@update');
Route::delete('genrebd/{id}', 'UserGenreController@delete');

// LOGIN 
Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');
Route::post('auth/recover', 'AuthController@recover');
Route::get('auth/user ', 'AuthController@user');


Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');
    Route::get('test', function(){
        return response()->json(['foo'=>'bar', 'hello' => 'hola']);
    });

    // Users 
    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@show');
    Route::post('users', 'UserController@store');
    Route::put('users/{id}', 'UserController@update');
    Route::delete('users/{id}', 'UserController@delete');

    // BD 
    Route::get('bd', 'BdController@index');
    Route::get('bd/{id}', 'BdController@show');
    Route::post('bd', 'BdController@store');
    Route::put('bd/{id}', 'BdController@update');
    Route::delete('bd/{id}', 'BdController@delete');

    // EVENTS 
    Route::get('events', 'EventsController@index');
    Route::get('events/{id}', 'EventsController@show');
    Route::post('events', 'EventsController@store');
    Route::put('events/{id}', 'EventsController@update');
    Route::delete('events/{id}', 'EventsController@delete');


    // COLLECTIONS
    Route::get('collections', 'CollectionsController@index');
    Route::get('collections/{id}', 'CollectionsController@show');
    Route::post('collections', 'CollectionsController@store');
    Route::put('collections/{id}', 'CollectionsController@update');
    Route::delete('collections/{id}', 'CollectionsController@delete');


    // Object
    Route::get('object', 'ObjectController@index');
    Route::get('object/{id}', 'ObjectController@show');
    Route::post('object', 'ObjectController@store');
    Route::put('object/{id}', 'ObjectController@update');
    Route::delete('object/{id}', 'ObjectController@delete');
});
