module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'auth',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast',
    'bootstrap-vue/nuxt'
  ],
  toast: {
    position: 'top-right',
    duration: 2000
  },
  axios: {
    baseURL: 'http://localhost:8000/api/',
    credentials: false
  },
  loading: {
    name: 'chasing-dots',
    color: '#000000',
    background: 'white',
    height: '4px'
 },
 auth: {
  strategies: {
    local: {
      endpoints: {
        login: {url: 'auth/login', method: 'post', propertyName: 'data.token' },
        logout: false,
        user: {url: 'auth/user', method: 'get', propertyName: 'data'},
      },
      tokenRequired: true,
      tokenType: 'Bearer'
    },
    facebook: {
      client_id: 'your facebook app id',
      userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=about,name,picture{url},email',
      scope: ['public_profile', 'email']
    },
    google: {
      client_id: 'your gcloud oauth app client id'
    },
    storageTokenName: 'nuxt-auth-token',
    tokenType: 'Bearer',
    notLoggedInRedirectTo: '/',
    loggedInRedirectTo: '/profile'
  },
  // redirect: {
  //   login: 'login',
  //   logout: 'logout',
  //   user: 'users/',
  //   callback:'/'
  // },
  /*
  ** Customize the progress bar color
  */
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
 }, 
 
}

